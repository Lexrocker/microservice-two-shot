from ast import Mod
from django.http import JsonResponse
from django.shortcuts import render
from .models import Shoe, BinVO
from common.json import ModelEncoder
import json
from django.views.decorators.http import require_http_methods
# Create your views here.


class ShoeListEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "id",
        "manufacturer",
        "model_name",
        "color",
        "photo_url",
        "bin",
    ]

    def get_extra_data(self, o):
        return {"bin": o.bin.closet_name}


@require_http_methods(["GET", "POST"])
def api_list_shoes(request):
    
    if request.method == "GET":
        shoes = Shoe.objects.all()
        return JsonResponse(
            {"shoes": shoes},
            encoder=ShoeListEncoder,
        )
    # elif request.method == "DELETE":
    #     count, _ = Shoe.objects.filter(id=pk).delete()
    #     return JsonResponse({"deleted": count > 0}) 
    else:
        content = json.loads(request.body)

        try:
            bin_id = content["bin"]
            bin = BinVO.objects.get(pk=bin_id)
            content["bin"] = bin
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid bin id"},
                status=400,
            )
    shoe = Shoe.objects.create(**content)
    return JsonResponse(
            shoe,
            encoder=ShoeListEncoder,
            safe=False,
        )

@require_http_methods(["DELETE"])
def api_delete_shoes(request, pk):
    if request.method == "DELETE":
        count, _ = Location.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})