from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from .models import Shoe, BinVO



@admin.register(Shoe)
class ShoeAdmin(admin.ModelAdmin):
    pass

@admin.register(BinVO)
class BinVO(admin.ModelAdmin):
    pass

