import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import HatForm from './HatCreate';
import Nav from './Nav';

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
          <Routes>
            <Route path="/" element={<MainPage />} />
            <Route path="/hats" element={<HatForm />} />
          </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
