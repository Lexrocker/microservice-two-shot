from django.urls import path

from .views import api_list_hats, api_list_hat

urlpatterns = [
    path('hats/', api_list_hats, name='api_hat_list_get_or_post' ),
    path('hats/<int:pk>/', api_list_hat, name='api_hat_delete' ),
]